PWD=$(shell pwd)
TAG=$(shell date +%y%m%d%H%M)

# Example of how to set a compile switch. Set it equal to non-zero value
EXAMPLE_COMPILE_SWITCH=0

all: debug

debug:
	mkdir -p dist
	mkdir -p build_debug
	cd build_debug; cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX:PATH=$(PWD)/dist ../src -DEXAMPLE_COMPILE_SWITCH=$(EXAMPLE_COMPILE_SWITCH)
	cd build_debug; make
	cd build_debug; make install

release:
	mkdir -p dist
	mkdir -p build_release
	cd build_release; cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=$(PWD)/dist ../src -DEXAMPLE_COMPILE_SWITCH=$(EXAMPLE_COMPILE_SWITCH)
	cd build_release; make
	cd build_release; make install

unittest:
	time LD_LIBRARY_PATH=dist/lib dist/bin/UnitTest 2>&1

clean:
	rm -rf dist/bin dist/lib build_debug build_release
#	rm -rf dist/bin build_debug build_release

documentation:
	mkdir -p dist/doc/pub
	mkdir -p dist/doc/internal
	doxygen src/Doxyfile

install:
	cd build_debug; make install
