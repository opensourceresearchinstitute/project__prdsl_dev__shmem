/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__WL_H
#define	_PR__WL_H
#define _GNU_SOURCE

#include <PR__include/PR__primitive_data_types.h>
#include <PR__include/PR__structs__common.h>
//=========================  Function Prototypes  ===========================
/* MEANING OF   WL  PI  SS  int PROS
 * These indicate which places the function is safe to use.  They stand for:
 * 
 * WL   Wrapper Library -- wrapper lib code should only use these
 * PI   Plugin          -- plugin code should only use these
 * SS   Startup and Shutdown -- designates these relate to startup & shutdown
 * int32internal to PR -- should not be used in wrapper lib or plugin
 * PROS means "OS functions for applications to use"
 * 
 * PR_int__ functions touch internal PR data structs and are only safe
 *  to be used inside the master lock.  However, occasionally, they appear
 * in wrapper-lib or plugin code.  In those cases, very careful analysis
 * has been done to be sure no concurrency issues could arise.
 * 
 * PR_WL__ functions are all safe for use outside the master lock.
 * 
 * PROS are only safe for applications to use -- they're like a second
 * language mixed in -- but they can't be used inside plugin code, and
 * aren't meant for use in wrapper libraries, because they are themselves
 * wrapper-library calls!
 */

//============== Top level Fns called from main   ===============
void
PR__start();

PRProcess *
PR__create_process( BirthFnPtr seed_Fn, void *seedData );

void
PR__end_seedVP( SlaveVP *seedSlv );

void
PR__end_process_from_inside( SlaveVP *seedSlv );

void *
PR__give_results_from_process_when_ready( PRProcess *process );

void
PR__wait_for_process_to_end( PRProcess *process );

void
PR__wait_for_all_activity_to_end();

void
PR__shutdown();

#define \
PR__create_taskID_of_size   PR_WL__create_taskID_of_size

inline
int32 *
PR__give_ID_from_slave( SlaveVP *animSlv, int32 magicNumber );

inline
int32 *
PR__give_ID_from_lang_meta_task( void *_task );


//============== include internally used fn prototypes ================
#include "PR__int.h"

#define PR_WL__give_lang_data  PR_int__give_lang_data_from_slave

#define \
PR_WL__create_slaveVP   PR_int__create_slaveVP_helper

#define \
PR_WL__give_lang_meta_task_from_slave   PR_int__give_lang_meta_task_from_slave

#define \
PR_WL__give_prolog_of_lang_meta_task   PR_int__give_prolog_of_lang_meta_task

//==============  Request Related  ===============

void
PR_WL__suspend_slaveVP_and_send_req( SlaveVP *callingSlv );

inline void
PR_WL__add_lang_request_in_mallocd_PRReqst( void *langReqData, 
                                                          SlaveVP *callingSlv );

inline void
PR_WL__send_lang_request( void *langReq, RequestHandler handler, 
                                          SlaveVP *callingSlv, int32 magicNum );

void
PR_WL__send_create_slaveVP_req( void *langReq, int32 *ID, CreateHandler handler, 
                                SlaveVP *reqstingSlv, int32 magicNum );

void inline
PR_WL__send_end_slave_req( void *langReq, RequestHandler handler, 
                                       SlaveVP *slvToDissipate, int32 magicNum );

inline void
PR_WL__send_service_request( void *langReqData, SlaveVP *callingSlv );

inline void
PR_WL__send_lang_shutdown_request( SlaveVP *callingSlv, int32 magicNum );

inline 
int32 *
PR_WL__create_taskID_of_size( int32 numInts );

//======================== MEASUREMENT ======================
uint64
PR_WL__give_num_plugin_cycles();
uint32
PR_WL__give_num_plugin_animations();


//========================= Utilities =======================
void
PR_WL__throw_exception( char *msgStr, SlaveVP *reqstSlv,  PRExcp *excpData );
#define PR_App__throw_exception PR_WL__throw_exception


//=======================================================================

//========================= Services =======================
//#include "Services_Offered_by_PR/Measurement_and_Stats/probes.h"
//#include  "Services_Offered_by_PR/Services_Language/PRServ.h"
//#include  "Services_Offered_by_PR/Services_Language/libPRServ.h"

//================================================
#endif	/* _PR_H */

