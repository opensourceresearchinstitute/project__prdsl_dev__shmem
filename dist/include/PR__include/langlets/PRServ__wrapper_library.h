/*
 *  Copyright 2009-2013 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRServ_wrapper_library_H
#define	_PRServ_wrapper_library_H

#include <PR__include/PR__structs__common.h>


//===========================================================================
typedef void (*PtrToAtomicFn )   ( void * ); //executed atomically in master

typedef struct _DKUPiece DKUPiece;
typedef struct _DKUInstance DKUInstance;

typedef void (*DKUKernel )       ( void *, SlaveVP * ); //used as task birth Fn
typedef void (*DKUSerialKernel ) ( void *, SlaveVP * ); //used as task birth Fn
typedef void (*DKUDivider )      ( DKUPiece * ); 
typedef void (*DKUUndivider )    ( DKUPiece * );

typedef DKUPiece *  (*DKURootPieceMaker) ( void *, DKUInstance * ); 

struct _DKUInstance
 { 
   DKURootPieceMaker rootPieceMaker;
   DKUKernel         kernel;
   DKUSerialKernel   serialKernel;
   DKUDivider        divider;
   DKUUndivider      undivider;
 };

struct _DKUPiece
 {
   void        *payload;
   int32        dataFootprint; //Kbytes of cache space used by piece -- set by root piece maker and then by divider
   DKUPiece    *parent;
   DKUPiece   **children;
   int32        childFootprint; //set as suggestion by PR, reset by divider
   int32        numChildren; //set as suggestion by PR, reset by divider
   int32        numUnfinishedChildren;
   DKUInstance *dkuInstance;   //to get kernel and undivider
   void        *undividerInfo; //divider communicates to undivider
   SlaveVP     *waitingVP;
   bool32       wasRecursivelyDivided;
 };
 

   
   
   
   
   

/*WARNING: assembly hard-codes position of endInstrAddr as first field
 */
typedef struct
 {
   void           *endInstrAddr;
   int32           hasBeenStarted;
   int32           hasFinished;
   PrivQueueStruc *waitQ;
 }
PRServSingleton;

 
//===========================================================================

int32
PRServ__giveMinWorkUnitCycles( float32 percentOverhead );

void
PRServ__begin_primitive();

int32
PRServ__end_primitive_and_give_cycles();

int32
PRServ__giveIdealNumWorkUnits();

int32
PRServ__give_number_of_cores_to_schedule_onto();

char *
PRServ___give_environment_string();

//=======================

void
PRServ__end_seedVP( SlaveVP *seedSlv );
//=======================

inline int32 *
PRServ__create_taskID_of_size( int32 numInts, SlaveVP *animSlv );

//=========================
void
PRServ__taskwait(SlaveVP *animSlv);

inline int32 *
PRServ__give_self_taskID( SlaveVP *animSlv );

//======================= Concurrency Stuff ======================
void
PRServ__start_fn_singleton( int32 singletonID, SlaveVP *animSlv );

void
PRServ__end_fn_singleton( int32 singletonID, SlaveVP *animSlv );

void
PRServ__start_data_singleton( PRServSingleton **singeltonAddr, SlaveVP *animSlv );

void
PRServ__end_data_singleton( PRServSingleton **singletonAddr, SlaveVP *animSlv );

void
PRServ__animate_short_fn_in_isolation( PtrToAtomicFn ptrToFnToExecInMaster,
                                    void *data, SlaveVP *animSlv );

void
PRServ__start_transaction( int32 transactionID, SlaveVP *animSlv );

void
PRServ__end_transaction( int32 transactionID, SlaveVP *animSlv );

//==============================  DKU  =============================
DKUInstance *
PRServ__DKU_make_empty_DKU_instance( SlaveVP *animSlv );

DKUPiece *
PRServ__DKU_make_empty_DKU_piece();

DKUPiece *
PRServ__DKU_make_child_piece_from( DKUPiece *pieceToDivide );

void
PRServ__DKU_set_root_piece_maker( DKUInstance *dkuInstance, 
                                  DKURootPieceMaker rootPieceMakerFn, 
                                  SlaveVP *animSlv );
void
PRServ__DKU_set_kernel( DKUInstance *dkuInstance, 
                        DKUKernel kernelFn, 
                        SlaveVP *animSlv );
void
PRServ__DKU_set_serial_kernel( DKUInstance *dkuInstance, 
                               DKUSerialKernel serialKernelFn, 
                               SlaveVP *animSlv );
void
PRServ__DKU_set_divider( DKUInstance *dkuInstance, 
                         DKUDivider dividerFn, 
                         SlaveVP *animSlv );
void
PRServ__DKU_set_undivider( DKUInstance *dkuInstance, 
                           DKUUndivider undividerFn, 
                           SlaveVP *animSlv );
DKUPiece *
PRServ__DKU_make_root_piece( DKUInstance *dkuInstance, 
                             void *data, 
                             SlaveVP *animSlv );
void
PRServ__DKU_perform_work_on( DKUPiece *rootPiece, 
                             SlaveVP *animSlv );
void
PRServ__DKU_wait_for_result_to_be_complete( DKUPiece *rootPiece, 
                                            SlaveVP  *animSlv );

//===========================================================================
#endif	/* _PRServ_H */

