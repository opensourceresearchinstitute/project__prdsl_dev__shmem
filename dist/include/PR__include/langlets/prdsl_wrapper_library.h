/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 */

#ifndef _PRDSL_WRAPPER_H
#define	_PRDSL_WRAPPER_H

#include <PR__include/PR__structs__common.h>

//===========================================================================
   //uniquely identifies PRDSL -- should be a jenkins char-hash of "PRDSL" to int32
#define PRDSL_MAGIC_NUMBER 0000000004

typedef struct _PRDSLTaskStub  PRDSLTaskStub;

//===========================================================================

/*This is PRDSL's "lang meta task"
 *See the proto-runtime wiki entry to learn about "lang meta task"
 *In essence, this holds all the meta information that PRDSL needs about a task
 */
struct _PRDSLTaskStub
 {
   void           **args;            //given to the birth Fn
   int32            numBlockingProp;
   PrivQueueStruc  *dependentTasksQ;
   bool32           isActive; //active after done adding propendents
   bool32           canBeAProp;
   
   PRDSLTaskStub *parentTaskStub; //for liveness, for the wait construct
   int32          numLiveChildTasks;
   int32          numLiveChildVPs;
   bool32         isWaitingForChildTasksToEnd;
   bool32         isWaitingForChildThreadsToEnd;
   bool32         isEnded;
   
//   int32         *taskID; //is in PRMetaTask, in prolog
 };


//=======================

void
PRDSL__start( SlaveVP *seedSlv );

void
PRDSL__shutdown( SlaveVP *seedSlv );

void
PRDSL__wait_for_all_PRDSL_created_work_to_end( SlaveVP *seedSlv );

//=======================

SlaveVP *
PRDSL__create_thread( BirthFnPtr fnPtr,   void *initData,
                                               SlaveVP *creatingThd );

void
PRDSL__end_thread( SlaveVP *thdToEnd );

//=======================

#define PRDSL__malloc( numBytes, callingSlave ) PR_App__malloc( numBytes, callingSlave)

#define PRDSL__free(ptrToFree, callingSlave ) PR_App__free( ptrToFree, callingSlave )


//=======================
PRDSLTaskStub *
PRDSL__create_task_ready_to_run( BirthFnPtr birthFn, void *args, SlaveVP *animSlv);

//inline int32 *
//PRDSL__create_taskID_of_size( int32 numInts, SlaveVP *animSlv );


void
PRDSL__end_task( SlaveVP *animSlv );

//=========================
void
PRDSL__taskwait(SlaveVP *animSlv);


inline int32 *
PRDSL__give_self_taskID( SlaveVP *animSlv );

//===========================================================================
#endif	

