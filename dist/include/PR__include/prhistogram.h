/*
 *  Copyright 2010 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */


#ifndef _PRHISTOGRAM_H
#define	_PRHISTOGRAM_H

#include <PR__include/prmalloc.h>
#include <PR__include/PR__primitive_data_types.h>


typedef struct
 {
   char  *name;
   int32  startOfRange;
   int32  endOfRange;
   int32  numBins;
   int32  binWidth;
   int32 *bins;
 }
Histogram;

typedef struct
 {
   float32  startOfRange;
   float32  endOfRange;
   int32    numBins;
   float32  binWidth;
   int32   *bins;
 }
FloatHist;

typedef struct
 {
   float64 startOfRange;
   float64  endOfRange;
   int32    numBins;
   float64  binWidth;
   int32   *bins;
 }
DblHist;

Histogram *
makeHistogram( int32 numBins, int32 startOfRange, int32 endOfRange );

Histogram *
makeFixedBinHist( int32 numBins, int32 startOfRange, int32 binWidth,
                  char *name );

Histogram *
makeFixedBinHistExt( int32 numBins, int32 startOfRange, int32 binWidth,
                     char *name );

void inline
addToHist( int32 value, Histogram *hist );

void inline
addIntervalToHist( uint32 startIntvl, uint32 endIntvl, Histogram *hist );

void inline
subIntervalFromHist( int32 startIntvl, int32 endIntvl, Histogram *hist );

void
saveHistToFile(Histogram *hist);

void
printHist( Histogram *hist );

FloatHist *
makeFloatHistogram( int numBins, float32 startOfRange, float32 binWidth );

void
addToFloatHist( float32 value, FloatHist *hist );

void
printFloatHist( FloatHist *hist );

void
freeHistExt( Histogram *hist );

void
freeHist( Histogram *hist );

DblHist *
makeDblHistogram( int numBins, float64 startOfRange, float64 binWidth );

void
addToDblHist( float64 value, DblHist *hist );

void
printDblHist( DblHist *hist );

void
freeDblHist( DblHist *hist );

#endif	/* _HISTOGRAM_H */

