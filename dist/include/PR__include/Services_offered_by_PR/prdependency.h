/*
 *  Copyright 2011 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: Nina Engelhardt
 * 
 */

#ifndef  _PRDEPENDENCY_H
#define	 _PRDEPENDENCY_H


#include <stdio.h>
#include <PR__include/prlistofarrays.h>

typedef struct {
    int vp;
    int task;
} Unit;

typedef struct {
    int from_vp;
    int from_task;
    int to_vp;
    int to_task;
} Dependency; 

typedef struct {
    int32 id;
    ListOfArrays* senders;
    ListOfArrays* receivers;
} NtoN;

FILE* dependency_file;

Dependency* new_dependency(int from_vp, int from_task, int to_vp, int to_task);

NtoN* new_NtoN(int id);

int set_dependency_file(FILE* file);

void print_ctl_dependency_to_file(void* _dep);

void print_comm_dependency_to_file(void* _dep);

void print_dyn_dependency_to_file(void* _dep);

void print_hw_dependency_to_file(void* _dep);

void print_dependency_to_file(void* dep);

void print_unit_to_file(void* unit);

void print_nton_to_file(void* _nton);

#endif	/* DEPENDENCY_H */

