/* 
 * File:   MEAS__Counter_Recording.h
 * Author: nengel
 *
 * Created on January 11, 2012, 3:03 PM
 */

#ifndef MEAS__COUNTER_RECORDING_H
#define	MEAS__COUNTER_RECORDING_H


#include <PR__include/PR__structs__common.h>

typedef struct 
 {
   int event_type;
   int coreID;
   AnimSlot* slot;
   int vp;
   int task;
   uint64 cycles;
   uint64 instrs;
 } 
CounterEvent;

FILE* counterfile; //pass file handle via side effect because
                   // doAllInListOfArrays only takes Fns with a single input

void MEAS__init_counter_data_structs_for_lang( SlaveVP *slv, int32 magicNum );

void MEAS__counter_handler(int evt_type, int vpid, int task, SlaveVP* pr, uint64 cycles, uint64 instrs);

void MEAS__set_counter_file(FILE* f);

void MEAS__print_counter_event_to_file( void* _e );
#endif	/* PRServ_COUNTER_RECORDING_H */

