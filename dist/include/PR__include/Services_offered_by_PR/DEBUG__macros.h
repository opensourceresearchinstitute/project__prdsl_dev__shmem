/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef  _DEBUG__macros_H
#define	_DEBUG__macros_H
#define _GNU_SOURCE

/*
 */
#ifdef DEBUG__TURN_ON_DEBUG_PRINT
   #define DEBUG__printf(  bool, ...) \
      do{\
         if(bool)\
          { printf(__VA_ARGS__);\
            printf(" | function: %s\n", __FUNCTION__);\
            fflush(stdin);\
          }\
        }while(0);/*macro magic to isolate var-names*/

   #define DEBUG__printf1( bool, msg, param)  \
      do{\
         if(bool)\
          { printf(msg, param);\
            printf(" | function: %s\n", __FUNCTION__);\
            fflush(stdin);\
          }\
        }while(0);/*macro magic to isolate var-names*/

   #define DEBUG__printf2( bool, msg, p1, p2) \
      do{\
         if(bool)\
          { printf(msg, p1, p2); \
            printf(" | function: %s\n", __FUNCTION__);\
            fflush(stdin);\
          }\
        }while(0);/*macro magic to isolate var-names*/

   #define DEBUG__printf3( bool, msg, p1, p2, p3) \
      do{\
         if(bool)\
          { printf(msg, p1, p2, p3); \
            printf(" | function: %s\n", __FUNCTION__);\
            fflush(stdin);\
          }\
        }while(0);/*macro magic to isolate var-names*/

#else
   #define DEBUG__printf(  bool, ...)         
   #define DEBUG__printf1( bool, msg, param)  
   #define DEBUG__printf2( bool, msg, p1, p2) 
#endif

//============================= ERROR MSGs ============================
#define ERROR(msg) printf(msg);
#define ERROR1(msg, param) printf(msg, param); 
#define ERROR2(msg, p1, p2) printf(msg, p1, p2);

//===========================================================================
#endif	/* _PR_DEFS_H */

