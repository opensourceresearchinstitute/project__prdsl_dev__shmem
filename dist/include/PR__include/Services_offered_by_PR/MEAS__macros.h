/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _MEAS_MACROS_H
#define _MEAS_MACROS_H
#define _GNU_SOURCE

//==================  Macros define types of meas want  =====================
//
/*Generic measurement macro -- has name-space collision potential, which
 * compiler will catch..  so only use one pair inside a given set of 
 * curly braces. 
 */
//TODO: finish generic capture interval in hist
enum histograms
 { generic1
 };
   #define MEAS__Capture_Pre_Point \
      int32 startStamp, endStamp; \
      saveLowTimeStampCountInto( startStamp );

   #define MEAS__Capture_Post_Point( histName ) \
      saveLowTimeStampCountInto( endStamp ); \
      addIntervalToHist( startStamp, endStamp, _PRTopEnv->histName ); 




//==================  Macros define types of meas want  =====================

#ifdef MEAS__TURN_ON_SUSP_MEAS
   #define MEAS__Insert_Susp_Meas_Fields_into_Slave \
       uint32  preSuspTSCLow; \
       uint32  postSuspTSCLow;

   #define MEAS__Insert_Susp_Meas_Fields_into_MasterEnv \
       Histogram       *suspLowTimeHist; \
       Histogram       *suspHighTimeHist;

   #define MEAS__Make_Meas_Hists_for_Susp_Meas \
      _PRTopEnv->suspLowTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "master_low_time_hist");\
      _PRTopEnv->suspHighTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "master_high_time_hist");
      
      //record time stamp: compare to time-stamp recorded below
   #define MEAS__Capture_Pre_Susp_Point \
      saveLowTimeStampCountInto( animatingSlv->preSuspTSCLow );
   
      //NOTE: only take low part of count -- do sanity check when take diff
   #define MEAS__Capture_Post_Susp_Point \
      saveLowTimeStampCountInto( animatingSlv->postSuspTSCLow );\
      addIntervalToHist( preSuspTSCLow, postSuspTSCLow,\
                         _PRTopEnv->suspLowTimeHist ); \
      addIntervalToHist( preSuspTSCLow, postSuspTSCLow,\
                         _PRTopEnv->suspHighTimeHist );

   #define MEAS__Print_Hists_for_Susp_Meas \
      printHist( _PRTopEnv->pluginTimeHist );
      
#else
   #define MEAS__Insert_Susp_Meas_Fields_into_Slave     
   #define MEAS__Insert_Susp_Meas_Fields_into_MasterEnv 
   #define MEAS__Make_Meas_Hists_for_Susp_Meas 
   #define MEAS__Capture_Pre_Susp_Point
   #define MEAS__Capture_Post_Susp_Point   
   #define MEAS__Print_Hists_for_Susp_Meas 
#endif

#ifdef MEAS__TURN_ON_MASTER_MEAS
   #define MEAS__Insert_Master_Meas_Fields_into_Slave \
       uint32  startMasterTSCLow; \
       uint32  endMasterTSCLow;

   #define MEAS__Insert_Master_Meas_Fields_into_MasterEnv \
       Histogram       *masterLowTimeHist; \
       Histogram       *masterHighTimeHist;

   #define MEAS__Make_Meas_Hists_for_Master_Meas \
      _PRTopEnv->masterLowTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "master_low_time_hist");\
      _PRTopEnv->masterHighTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "master_high_time_hist");

      //Total Master time includes one coreloop time -- just assume the core
      // loop time is same for Master as for AppSlvs, even though it may be
      // smaller due to higher predictability of the fixed jmp.
   #define MEAS__Capture_Pre_Master_Point\
      saveLowTimeStampCountInto( masterVP->startMasterTSCLow );

   #define MEAS__Capture_Post_Master_Point \
      saveLowTimeStampCountInto( masterVP->endMasterTSCLow );\
      addIntervalToHist( startMasterTSCLow, endMasterTSCLow,\
                         _PRTopEnv->masterLowTimeHist ); \
      addIntervalToHist( startMasterTSCLow, endMasterTSCLow,\
                         _PRTopEnv->masterHighTimeHist );

   #define MEAS__Print_Hists_for_Master_Meas \
      printHist( _PRTopEnv->pluginTimeHist );

#else
   #define MEAS__Insert_Master_Meas_Fields_into_Slave
   #define MEAS__Insert_Master_Meas_Fields_into_MasterEnv 
   #define MEAS__Make_Meas_Hists_for_Master_Meas
   #define MEAS__Capture_Pre_Master_Point 
   #define MEAS__Capture_Post_Master_Point 
   #define MEAS__Print_Hists_for_Master_Meas 
#endif

      
#ifdef MEAS__TURN_ON_MASTER_LOCK_MEAS
   #define MEAS__Insert_Master_Lock_Meas_Fields_into_MasterEnv \
       Histogram       *masterLockLowTimeHist; \
       Histogram       *masterLockHighTimeHist;

   #define MEAS__Make_Meas_Hists_for_Master_Lock_Meas \
      _PRTopEnv->masterLockLowTimeHist  = makeFixedBinHist( 50, 0, 2, \
                                               "master lock low time hist");\
      _PRTopEnv->masterLockHighTimeHist  = makeFixedBinHist( 50, 0, 100,\
                                               "master lock high time hist");

   #define MEAS__Capture_Pre_Master_Lock_Point \
      int32 startStamp, endStamp; \
      saveLowTimeStampCountInto( startStamp );

   #define MEAS__Capture_Post_Master_Lock_Point \
      saveLowTimeStampCountInto( endStamp ); \
      addIntervalToHist( startStamp, endStamp,\
                         _PRTopEnv->masterLockLowTimeHist ); \
      addIntervalToHist( startStamp, endStamp,\
                         _PRTopEnv->masterLockHighTimeHist );

   #define MEAS__Print_Hists_for_Master_Lock_Meas \
      printHist( _PRTopEnv->masterLockLowTimeHist ); \
      printHist( _PRTopEnv->masterLockHighTimeHist );
      
#else
   #define MEAS__Insert_Master_Lock_Meas_Fields_into_MasterEnv
   #define MEAS__Make_Meas_Hists_for_Master_Lock_Meas
   #define MEAS__Capture_Pre_Master_Lock_Point 
   #define MEAS__Capture_Post_Master_Lock_Point 
   #define MEAS__Print_Hists_for_Master_Lock_Meas
#endif


#ifdef MEAS__TURN_ON_MALLOC_MEAS
   #define MEAS__Insert_Malloc_Meas_Fields_into_MasterEnv\
       Histogram       *mallocTimeHist; \
       Histogram       *freeTimeHist;

   #define MEAS__Make_Meas_Hists_for_Malloc_Meas \
      _PRTopEnv->mallocTimeHist  = makeFixedBinHistExt( 100, 0, 30,\
                                                       "malloc_time_hist");\
      _PRTopEnv->freeTimeHist  = makeFixedBinHistExt( 100, 0, 30,\
                                                       "free_time_hist");

   #define MEAS__Capture_Pre_Malloc_Point \
      int32 startStamp, endStamp; \
      saveLowTimeStampCountInto( startStamp );

   #define MEAS__Capture_Post_Malloc_Point \
      saveLowTimeStampCountInto( endStamp ); \
      addIntervalToHist( startStamp, endStamp,\
                         _PRTopEnv->mallocTimeHist ); 

   #define MEAS__Capture_Pre_Free_Point \
      int32 startStamp, endStamp; \
      saveLowTimeStampCountInto( startStamp );

   #define MEAS__Capture_Post_Free_Point \
      saveLowTimeStampCountInto( endStamp ); \
      addIntervalToHist( startStamp, endStamp,\
                         _PRTopEnv->freeTimeHist ); 

   #define MEAS__Print_Hists_for_Malloc_Meas \
      printHist( _PRTopEnv->mallocTimeHist   ); \
      saveHistToFile( _PRTopEnv->mallocTimeHist   ); \
      printHist( _PRTopEnv->freeTimeHist     ); \
      saveHistToFile( _PRTopEnv->freeTimeHist     ); \
      freeHistExt( _PRTopEnv->mallocTimeHist ); \
      freeHistExt( _PRTopEnv->freeTimeHist   );
      
#else
   #define MEAS__Insert_Malloc_Meas_Fields_into_MasterEnv
   #define MEAS__Make_Meas_Hists_for_Malloc_Meas 
   #define MEAS__Capture_Pre_Malloc_Point
   #define MEAS__Capture_Post_Malloc_Point
   #define MEAS__Capture_Pre_Free_Point
   #define MEAS__Capture_Post_Free_Point
   #define MEAS__Print_Hists_for_Malloc_Meas 
#endif



#ifdef MEAS__TURN_ON_PLUGIN_MEAS 
   #define MEAS__Insert_Plugin_Meas_Fields_into_MasterEnv \
      Histogram       *reqHdlrLowTimeHist; \
      Histogram       *reqHdlrHighTimeHist;
          
   #define MEAS__Make_Meas_Hists_for_Plugin_Meas \
      _PRTopEnv->reqHdlrLowTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "plugin_low_time_hist");\
      _PRTopEnv->reqHdlrHighTimeHist  = makeFixedBinHistExt( 100, 0, 200,\
                                                    "plugin_high_time_hist");

   #define MEAS__startReqHdlr \
      int32 startStamp1, endStamp1; \
      saveLowTimeStampCountInto( startStamp1 );

   #define MEAS__endReqHdlr \
      saveLowTimeStampCountInto( endStamp1 ); \
      addIntervalToHist( startStamp1, endStamp1, \
                           _PRTopEnv->reqHdlrLowTimeHist ); \
      addIntervalToHist( startStamp1, endStamp1, \
                           _PRTopEnv->reqHdlrHighTimeHist );

   #define MEAS__Print_Hists_for_Plugin_Meas \
      printHist( _PRTopEnv->reqHdlrLowTimeHist ); \
      saveHistToFile( _PRTopEnv->reqHdlrLowTimeHist ); \
      printHist( _PRTopEnv->reqHdlrHighTimeHist ); \
      saveHistToFile( _PRTopEnv->reqHdlrHighTimeHist ); \
      freeHistExt( _PRTopEnv->reqHdlrLowTimeHist ); \
      freeHistExt( _PRTopEnv->reqHdlrHighTimeHist );
#else
   #define MEAS__Insert_Plugin_Meas_Fields_into_MasterEnv
   #define MEAS__Make_Meas_Hists_for_Plugin_Meas
   #define MEAS__startReqHdlr 
   #define MEAS__endReqHdlr 
   #define MEAS__Print_Hists_for_Plugin_Meas 

#endif

      
#ifdef MEAS__TURN_ON_SYSTEM_MEAS
   #define MEAS__Insert_System_Meas_Fields_into_Slave \
      TSCountLowHigh  startSusp; \
      uint64  totalSuspCycles; \
      uint32  numGoodSusp;

   #define MEAS__Insert_System_Meas_Fields_into_MasterEnv \
       TSCountLowHigh   startMaster; \
       uint64           totalMasterCycles; \
       uint32           numMasterAnimations; \
       TSCountLowHigh   startReqHdlr; \
       uint64           totalPluginCycles; \
       uint32           numPluginAnimations; \
       uint64           cyclesTillStartAnimationMaster; \
       TSCountLowHigh   endAnimationMaster;

   #define MEAS__startAnimationMaster_forSys \
      TSCountLowHigh startStamp1, endStamp1; \
      saveTSCLowHigh( endStamp1 ); \
      _PRTopEnv->cyclesTillStartAnimationMaster = \
      endStamp1.longVal - masterVP->startSusp.longVal;

   #define Meas_startReqHdlr_forSys \
        saveTSCLowHigh( startStamp1 ); \
        _PRTopEnv->startReqHdlr.longVal = startStamp1.longVal;
 
   #define MEAS__endAnimationMaster_forSys \
      saveTSCLowHigh( startStamp1 ); \
      _PRTopEnv->endAnimationMaster.longVal = startStamp1.longVal;

   /*A TSC is stored in VP first thing inside wrapper-lib
    * Now, measures cycles from there to here
    * Master and Plugin will add this value to other trace-seg measures
    */
   #define MEAS__Capture_End_Susp_in_CoreCtlr_ForSys\
          saveTSCLowHigh(endSusp); \
          numCycles = endSusp.longVal - currVP->startSusp.longVal; \
          /*sanity check (400K is about 20K iters)*/ \
          if( numCycles < 400000 ) \
           { currVP->totalSuspCycles += numCycles; \
             currVP->numGoodSusp++; \
           } \
             /*recorded every time, but only read if currVP == MasterVP*/ \
          _PRTopEnv->startMaster.longVal = endSusp.longVal;

#else
   #define MEAS__Insert_System_Meas_Fields_into_Slave 
   #define MEAS__Insert_System_Meas_Fields_into_MasterEnv 
   #define MEAS__Make_Meas_Hists_for_System_Meas
   #define MEAS__startAnimationMaster_forSys 
   #define MEAS__startReqHdlr_forSys
   #define MEAS__endAnimationMaster_forSys
   #define MEAS__Capture_End_Susp_in_CoreCtlr_ForSys
   #define MEAS__Print_Hists_for_System_Meas 
#endif

#ifdef HOLISTIC__TURN_ON_PERF_COUNTERS
   
   #define MEAS__Insert_Counter_Handler \
   typedef void (*CounterHandler) (int,int,int,SlaveVP*,uint64,uint64,uint64);
 
   enum eventType {
    DebugEvt = 0,
    AppResponderInvocation_start,
    AppResponder_start,
    AppResponder_end,
    AssignerInvocation_start,
    NextAssigner_start,
    Assigner_start,
    Assigner_end,
    Work_start,
    Work_end,
    HwResponderInvocation_start,
    Timestamp_start,
    Timestamp_end
   };
   
   #define saveCyclesAndInstrs(core,cycles,instrs,cachem) do{ \
   int cycles_fd = _PRTopEnv->cycles_counter_fd[core]; \
   int instrs_fd = _PRTopEnv->instrs_counter_fd[core]; \
   int cachem_fd = _PRTopEnv->cachem_counter_fd[core]; \
   int nread;                                           \
                                                        \
   nread = read(cycles_fd,&(cycles),sizeof(cycles));    \
   if(nread<0){                                         \
       perror("Error reading cycles counter");          \
       cycles = 0;                                      \
   }                                                    \
                                                        \
   nread = read(instrs_fd,&(instrs),sizeof(instrs));    \
   if(nread<0){                                         \
       perror("Error reading cycles counter");          \
       instrs = 0;                                      \
   }                                                    \
   nread = read(cachem_fd,&(cachem),sizeof(cachem));    \
   if(nread<0){                                         \
       perror("Error reading last level cache miss counter");          \
       cachem = 0;                                      \
   }                                                    \
   } while (0) 

   #define MEAS__Insert_Counter_Meas_Fields_into_MasterEnv \
     int cycles_counter_fd[NUM_CORES]; \
     int instrs_counter_fd[NUM_CORES]; \
     int cachem_counter_fd[NUM_CORES]; \
     uint64 start_master_lock[NUM_CORES][3]; \
     CounterHandler counterHandler;

   #define HOLISTIC__Setup_Perf_Counters setup_perf_counters();
   

   #define HOLISTIC__CoreCtrl_Setup \
   CounterHandler counterHandler = _PRTopEnv->counterHandler; \
   SlaveVP      *lastVPBeforeMaster = NULL; \
   /*if(thisCoresThdParams->coreNum == 0){ \
       uint64 initval = tsc_offset_send(thisCoresThdParams,0); \
       while(!coreCtlrThdParams[NUM_CORES - 2]->ret_tsc); \
   } \
   if(0 < (thisCoresThdParams->coreNum) && (thisCoresThdParams->coreNum) < (NUM_CORES - 1)){ \
       ThdParams* sendCoresThdParams = coreCtlrThdParams[thisCoresThdParams->coreNum - 1]; \
       int sndctr = tsc_offset_resp(sendCoresThdParams, 0); \
       uint64 initval = tsc_offset_send(thisCoresThdParams,0); \
       while(!coreCtlrThdParams[NUM_CORES - 2]->ret_tsc); \
   }  \
   if(thisCoresThdParams->coreNum == (NUM_CORES - 1)){ \
       ThdParams* sendCoresThdParams = coreCtlrThdParams[thisCoresThdParams->coreNum - 1]; \
       int sndctr = tsc_offset_resp(sendCoresThdParams,0); \
   }*/
   
   
   #define HOLISTIC__Insert_Master_Global_Vars \
        int vpid,task; \
        CounterHandler counterHandler = _PRTopEnv->counterHandler;
   
   #define HOLISTIC__Record_last_work lastVPBeforeMaster = currVP;

   #define HOLISTIC__Record_AppResponderInvocation_start \
      uint64 cycles,instrs,cachem; \
      saveCyclesAndInstrs(thisCoresIdx,cycles, instrs,cachem); \
      if(lastVPBeforeMaster){ \
        (*counterHandler)(AppResponderInvocation_start,lastVPBeforeMaster->slaveNum,lastVPBeforeMaster->numTimesAssignedToASlot,lastVPBeforeMaster,cycles,instrs,cachem); \
        lastVPBeforeMaster = NULL; \
      } else { \
          _PRTopEnv->start_master_lock[thisCoresIdx][0] = cycles; \
          _PRTopEnv->start_master_lock[thisCoresIdx][1] = instrs; \
          _PRTopEnv->start_master_lock[thisCoresIdx][2] = cachem; \
      }
 
           /* Request Handler may call resume() on the VP, but we want to 
                * account the whole interval to the same task. Therefore, need
                * to save task ID at the beginning.
                * 
                * Using this value as "end of AppResponder Invocation Time"
                * is possible if there is only one SchedSlot per core -
                * invoking processor is last to be treated here! If more than
                * one slot, MasterLoop processing time for all but the last VP
                * would be erroneously counted as invocation time.
                */
   #define HOLISTIC__Record_AppResponder_start \
               vpid = currSlot->slaveAssignedToSlot->slaveNum; \
               task = currSlot->slaveAssignedToSlot->numTimesAssignedToASlot; \
               uint64 cycles, instrs, cachem; \
               saveCyclesAndInstrs(thisCoresIdx,cycles, instrs,cachem); \
               (*counterHandler)(AppResponder_start,vpid,task,currSlot->slaveAssignedToSlot,cycles,instrs,cachem);

   #define HOLISTIC__Record_AppResponder_end \
        uint64 cycles2,instrs2,cachem2; \
        saveCyclesAndInstrs(thisCoresIdx,cycles2, instrs2,cachem2); \
        (*counterHandler)(AppResponder_end,vpid,task,currSlot->slaveAssignedToSlot,cycles2,instrs2,cachem2); \
        (*counterHandler)(Timestamp_end,vpid,task,currSlot->slaveAssignedToSlot,rdtsc(),0,0);

   
   /* Don't know who to account time to yet - goes to assigned VP
    * after the call.
    */
   #define HOLISTIC__Record_Assigner_start \
       int empty = FALSE; \
       if(currSlot->slaveAssignedToSlot == NULL){ \
           empty= TRUE; \
       } \
       uint64 tmp_cycles, tmp_instrs, tmp_cachem; \
       saveCyclesAndInstrs(thisCoresIdx,tmp_cycles,tmp_instrs,tmp_cachem); \
       uint64 tsc = rdtsc(); \
       if(vpid > 0) { \
           (*counterHandler)(NextAssigner_start,vpid,task,currSlot->slaveAssignedToSlot,tmp_cycles,tmp_instrs,tmp_cachem); \
           vpid = 0; \
           task = 0; \
        }

   #define HOLISTIC__Record_Assigner_end \
        uint64 cycles,instrs,cachem; \
        saveCyclesAndInstrs(thisCoresIdx,cycles,instrs,cachem); \
        if(empty){ \
            (*counterHandler)(AssignerInvocation_start,assignedSlaveVP->slaveNum,assignedSlaveVP->numTimesAssignedToASlot,assignedSlaveVP,_PRTopEnv->start_master_lock[thisCoresIdx][0],_PRTopEnv->start_master_lock[thisCoresIdx][1],masterEnv->start_master_lock[thisCoresIdx][2]); \
        } \
        (*counterHandler)(Timestamp_start,assignedSlaveVP->slaveNum,assignedSlaveVP->numTimesAssignedToASlot,assignedSlaveVP,tsc,0,0); \
        (*counterHandler)(Assigner_start,assignedSlaveVP->slaveNum,assignedSlaveVP->numTimesAssignedToASlot,assignedSlaveVP,tmp_cycles,tmp_instrs,tmp_cachem); \
        (*counterHandler)(Assigner_end,assignedSlaveVP->slaveNum,assignedSlaveVP->numTimesAssignedToASlot,assignedSlaveVP,cycles,instrs,tmp_cachem);

   #define HOLISTIC__Record_Work_start \
        if(currVP){ \
                uint64 cycles,instrs,cachem; \
                saveCyclesAndInstrs(thisCoresIdx,cycles, instrs,cachem); \
                (*counterHandler)(Work_start,currVP->slaveNum,currVP->numTimesAssignedToASlot,currVP,cycles,instrs,cachem); \
        }
   
   #define HOLISTIC__Record_Work_end \
       if(currVP){ \
               uint64 cycles,instrs,cachem; \
               saveCyclesAndInstrs(thisCoresIdx,cycles, instrs,cachem); \
               (*counterHandler)(Work_end,currVP->slaveNum,currVP->numTimesAssignedToASlot,currVP,cycles,instrs,cachem); \
       }

   #define HOLISTIC__Record_HwResponderInvocation_start \
        uint64 cycles,instrs,cachem; \
        saveCyclesAndInstrs(animatingSlv->coreAnimatedBy,cycles, instrs,cachem); \
        (*(_PRTopEnv->counterHandler))(HwResponderInvocation_start,animatingSlv->slaveNum,animatingSlv->numTimesAssignedToASlot,animatingSlv,cycles,instrs,cachem); 
        

   #define getReturnAddressBeforeLibraryCall(vp_ptr, res_ptr) do{     \
void* frame_ptr0 = vp_ptr->framePtr;                               \
void* frame_ptr1 = *((void**)frame_ptr0);                          \
void* frame_ptr2 = *((void**)frame_ptr1);                          \
void* frame_ptr3 = *((void**)frame_ptr2);                          \
void* ret_addr = *((void**)frame_ptr3 + 1);                        \
*res_ptr = ret_addr;                                               \
} while (0)

#else  
   #define MEAS__Insert_Counter_Handler
   #define MEAS__Insert_Counter_Meas_Fields_into_MasterEnv
   #define HOLISTIC__Setup_Perf_Counters
   #define HOLISTIC__CoreCtrl_Setup
   #define HOLISTIC__Insert_Master_Global_Vars
   #define HOLISTIC__Record_last_work
   #define HOLISTIC__Record_AppResponderInvocation_start
   #define HOLISTIC__Record_AppResponder_start
   #define HOLISTIC__Record_AppResponder_end
   #define HOLISTIC__Record_Assigner_start
   #define HOLISTIC__Record_Assigner_end
   #define HOLISTIC__Record_Work_start
   #define HOLISTIC__Record_Work_end
   #define HOLISTIC__Record_HwResponderInvocation_start
   #define getReturnAddressBeforeLibraryCall(vp_ptr, res_ptr)
#endif

//Experiment in two-step macros -- if doesn't work, insert each separately
#define MEAS__Insert_Meas_Fields_into_Slave  \
   MEAS__Insert_Susp_Meas_Fields_into_Slave \
   MEAS__Insert_Master_Meas_Fields_into_Slave \
   MEAS__Insert_System_Meas_Fields_into_Slave 


//======================  Histogram Macros -- Create ========================
//
//

//The language implementation should include a definition of this macro,
// which creates all the histograms the language uses to collect measurements
// of plugin operation -- so, if the language didn't define it, must
// define it here (as empty), to avoid compile error
#ifndef MEAS__Make_Meas_Hists_for_Language
#define MEAS__Make_Meas_Hists_for_Language
#endif

#define makeAMeasHist( histInfo, idx, name, numBins, startVal, binWidth ) \
      makeHighestDynArrayIndexBeAtLeast( _PRTopEnv->measHistsInfo, idx ); \
      _PRTopEnv->measHists[idx] =  \
                       makeFixedBinHist( numBins, startVal, binWidth, name );

//==============================  Probes  ===================================


//===========================================================================
#endif	/* _PR_DEFS_MEAS_H */

