/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 * 
 */

#ifndef _PR__PI_H
#define	_PR__PI_H
#define _GNU_SOURCE

#include <PR__include/PR__structs__common.h>

//=========================  Function Prototypes  ===========================
/* MEANING OF   WL  PI  SS  int PROS
 * These indicate which places the function is safe to use.  They stand for:
 * 
 * WL   Wrapper Library -- wrapper lib code should only use these
 * PI   Plugin          -- plugin code should only use these
 * SS   Startup and Shutdown -- designates these relate to startup & shutdown
 * int32internal to PR -- should not be used in wrapper lib or plugin
 * PROS means "OS functions for applications to use"
 * 
 * PR_int__ functions touch internal PR data structs and are only safe
 *  to be used inside the master lock.  However, occasionally, they appear
 * in wrapper-lib or plugin code.  In those cases, very careful analysis
 * has been done to be sure no concurrency issues could arise.
 * 
 * PR_WL__ functions are all safe for use outside the master lock.
 * 
 * PROS are only safe for applications to use -- they're like a second
 * language mixed in -- but they can't be used inside plugin code, and
 * aren't meant for use in wrapper libraries, because they are themselves
 * wrapper-library calls!
 */

#define \
PR_PI__create_slaveVP   PR_int__create_slaveVP_helper

//==============
//=== Lang Data
//=
#define \
PR_PI__give_lang_data_from_slave   PR_int__give_lang_data_from_slave
#define \
PR_SS__give_lang_data_from_slave   PR_int__give_lang_data_from_slave

#define \
PR_PI__give_num_cores  PR_SS__give_num_cores


//============
//=== Lang Env
//=
#define \
PR_PI__give_proto_lang_env_for_slave   PR_int__give_proto_lang_env_for_slave
#define \
PR_PI__give_lang_env_for_slave   PR_int__give_lang_env_for_slave
#define \
PR_PI__give_lang_env_from_process   PR_int__give_lang_env_from_process


//=========
//=== Meta Task
//=
#define \
PR_PI__give_lang_meta_task_from_slave    PR_int__give_lang_meta_task_from_slave
#define \
PR_PI__give_prolog_of_lang_meta_task   PR_int__give_prolog_of_lang_meta_task

SlaveVP *
PR_PI__give_slave_lang_meta_task_is_assigned_to( void *langMetaTask );

#define \
PR_PI__free_lang_meta_task_and_remove_from_slave    PR_int__free_lang_meta_task_and_remove_from_coll

#define \
PR_PI__free_lang_meta_task    PR_int__free_lang_meta_task

void
PR_PI__set_no_del_flag_in_lang_meta_task( void *langMetaTask );
void
PR_PI__clear_no_del_flag_in_lang_meta_task( void *langMetaTask );

//==========
//===
//=
#define \
PR_PI__give_ID_from_lang_meta_task   PR__give_ID_from_lang_meta_task
#define \
PR_PI__give_ID_from_slave   PR__give_ID_from_slave

//=============
//===
//=
#define \
PR_PI__put_slave_into_slot   PR_int__put_slave_into_slot
#define \
PR_PI__put_task_into_slot    PR_int__put_task_into_slot

PRReqst *
PR_PI__take_next_request_out_of( SlaveVP *slaveWithReq );

#define \
PR_PI__take_lang_reqst_from( req )   req->langReq

void
PR_PI__resume_slave_in_PRServ( SlaveVP *slave );

#define \
PR_PI__malloc   PR_int__malloc
#define \
PR_PI__malloc_aligned   PR_int__malloc_aligned
#define \
PR_PI__free  PR_int__free


#define \
PR_PI__throw_exception   PR_int__throw_exception

//================================================
#endif	/* _PR__PI_H */

