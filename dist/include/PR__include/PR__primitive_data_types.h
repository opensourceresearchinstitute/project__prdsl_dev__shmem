/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *  
 * Author: seanhalle@yahoo.com
 *  

 */

#ifndef _PRIMITIVE_DATA_TYPES_H
#define _PRIMITIVE_DATA_TYPES_H


/*For portability, need primitive data types that have a well defined
 * size, and well-defined layout into bytes
 *To do this, provide standard aliases for all primitive data types
 *These aliases must be used in all functions instead of the ANSI types
 *
 *When PR is used together with BLIS, these definitions will be replaced
 * inside each specialization module according to the compiler used in
 * that module and the hardware being specialized to.
 */
typedef char               bool8;
typedef char               int8;
typedef char               uint8;
typedef short              int16;
typedef unsigned short     uint16;
typedef int                int32;
typedef unsigned int       uint32;
typedef unsigned int       bool32;
typedef long long          int64;
typedef unsigned long long uint64;
typedef float              float32;
typedef double             float64;
//typedef double double      float128;  //GCC doesn't like this
#define float128 double double

#define TRUE  1
#define FALSE 0

#endif	/* _PRIMITIVE_DATA_TYPES_H */

